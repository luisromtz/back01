package productoAPI.servicios;

import org.springframework.stereotype.Service;
import productoAPI.modelo.ProductoModelo;
import productoAPI.modelo.ModeloUsuario;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.List;

@Service

public class ProductoServicio {


    private List<ProductoModelo> dataList = new ArrayList<ProductoModelo>();

    public ProductoServicio(){
        dataList.add(new ProductoModelo(1, "producto 1", 100.50));
        dataList.add(new ProductoModelo(2, "producto 2", 150.00));
        dataList.add(new ProductoModelo(3, "producto 3", 100.00));
        dataList.add(new ProductoModelo(4, "producto 4", 50.75));
        dataList.add(new ProductoModelo(5, "producto 5", 120.00));
        List<ModeloUsuario> users = new ArrayList<>();
        users.add(new ModeloUsuario("1"));
        users.add(new ModeloUsuario("3"));
        users.add(new ModeloUsuario("5"));
        dataList.get(1).setUsers(users);
    }


    //Leer colecciones
    public List<ProductoModelo> getProductos(){
        return dataList;
    }

    //Leer instancia
    public ProductoModelo getProducto(long index) throws IndexOutOfBoundsException{
        if(getIndex(index)>=0){
            return dataList.get(getIndex(index));
        }
        return null;
    }

    //CREAR
    public ProductoModelo addProducto(ProductoModelo newPro){
        dataList.add(newPro);
        return newPro;
    }

    //UPDATE
    public ProductoModelo updateProducto (int index, ProductoModelo newPro)
                        throws IndexOutOfBoundsException{
        dataList.set(index, newPro);
        return dataList.get(index);
    }

    //DELETE
    public void removeProducto(int index) throws IndexOutOfBoundsException{
        int pos = dataList.indexOf(dataList.get(index));
        dataList.remove(pos);
    }

    //GET index
    public int getIndex(long index) throws IndexOutOfBoundsException{
        int i=0;
        while(i<dataList.size()){
            if(dataList.get(i).getId() == index){
                return(i);}
            i++;
        }
        return -1;
    }
}
