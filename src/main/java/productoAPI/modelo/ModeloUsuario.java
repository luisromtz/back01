package productoAPI.modelo;

public class ModeloUsuario {
    private String userId;

    public ModeloUsuario(){}

    public ModeloUsuario(String userId){
        this.userId = userId;
    }

    public String getUserId(){
        return this.userId;
    }

}
